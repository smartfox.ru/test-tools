# test-tools

Tools for easy testing of std in/out small programs.
Designed primarily for sport or interview problems testing in individual projects.
Assertions based on junit library.