package org.smartfox.tools.test;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Samples.
 *
 * @author smartfox
 */
@UtilityClass
public class Samples {

    //todo eishutin: remove input console output for passed tests

    public static void run(String sample, String expect) {
        run(sample, expect, getTargetClass());
    }

    public static void runFiles(String sample, String expect) {
        run(readAsString(Samples.class.getResourceAsStream(sample)),
                readAsString(Samples.class.getResourceAsStream(expect)), getTargetClass());
    }

    @SneakyThrows
    private static String readAsString(InputStream input) {
        List<String> lines = IOUtils.readLines(input, Charset.forName("UTF-8"));
        return String.join("\n", lines);
    }

    public static void run(String sample, String expect, Class target) {
        System.out.println();
        System.out.println("running: " + Thread.currentThread().getStackTrace()[3].getMethodName());
        Samples.runSample(target, () -> sample, s -> {
            String[] split = s.split("\n");
            int z = 0;
            for (String s1 : split) {
                System.out.println(z++ + ": " + s1.trim());
            }
            String[] exp = expect.split("\n");
            for (int i = 0; i < exp.length; i++) {
                assertEquals(exp[i], split[i].trim(), "wrong answer at line: " + i);
            }
        }, "-debug");
    }

    private static Class<?> getTargetClass() {
        try {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            for (StackTraceElement e : stackTrace) {
                String className = e.getClassName();
                if (className.endsWith("Test")) {
                    Class type = Class.forName(className);
                    Method method;
                    try {
                        method = type.getMethod(e.getMethodName());
                        if (method.getAnnotation(Test.class) != null) {
                            return Class.forName(className.substring(0, className.length() - 4));
                        }
                    } catch (NoSuchMethodException e1) {
                    }
                }
            }
            throw new RuntimeException("no suitable class found in stacktrace");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @SneakyThrows
    public static void runSample(Class<?> main, Supplier<String> sampleDataSupplier, Consumer<String> resultCheckConsumer, String... args) {
        Method mainMethod = main.getDeclaredMethod("main", String[].class);
        System.setIn(new ByteArrayInputStream(sampleDataSupplier.get().getBytes("UTF-8")));
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream oldOut = System.out;
        try {
            System.setOut(new PrintStream(out));
            mainMethod.invoke(null, new Object[]{args});
        } finally {
            System.setOut(oldOut);
            out.close();
        }

        resultCheckConsumer.accept(new String(out.toByteArray(), "UTF-8"));
    }

    @SneakyThrows
    public static void runSample(Class<?> main, String sampleData, String expectedResult) {
        runSample(main, () -> sampleData, s -> assertEquals(expectedResult.trim(), s.trim()));
    }
}
